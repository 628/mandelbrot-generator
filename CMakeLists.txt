cmake_minimum_required(VERSION 3.15)
project(MandelbrotGenerator)

set(CMAKE_CXX_STANDARD 14)

add_executable(MandelbrotGenerator main.cpp complex.cpp include/complex.h mandelbrot.cpp include/mandelbrot.h include/image_t.h include/pixeldata.h imageProcessor.cpp include/imageProcessor.h image.cpp include/image.h include/palette.h palette.cpp commandHandler.cpp include/commandHandler.h)

set_target_properties(MandelbrotGenerator PROPERTIES COMPILE_FLAGS "-g -Wall -Wextra -Werror -pedantic")

target_link_libraries(MandelbrotGenerator png)