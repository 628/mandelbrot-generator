# Mandelbrot Generator
Mandelbrot-set image generator.

## Example Images
![mandelbrot](https://i.ibb.co/qJYQ0tz/mandelbrot.png)
![mandelbrot](https://i.ibb.co/s9KGwwY/mandelbrot.png)