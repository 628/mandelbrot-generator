#include "include/commandHandler.h"

void CommandHandler::registerArgument(std::string fullArgument,
                                      std::string abbreviation,
                                      std::function<std::string(metadataCluster_t *, std::string)> callback) {
    CommandArgument arg = CommandArgument(fullArgument, abbreviation, callback);
    arguments.push_back(arg);
}
