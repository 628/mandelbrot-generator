#include <cmath>
#include "include/complex.h"

double Complex::abs() {
    double a = std::pow(this->real, 2);
    double b = std::pow(this->imag, 2);

    return std::sqrt(a + b);
}

Complex &Complex::add(Complex *complex) {
    this->real = this->real + complex->real;
    this->imag = this->imag + complex->imag;

    return *this;
}

Complex &Complex::multi(Complex *complex) {
    long double newReal = (this->real * complex->real) - (this->imag * complex->imag);
    long double newImag = (this->real * complex->imag) + (this->imag * complex->real);

    this->real = newReal;
    this->imag = newImag;

    return *this;
}

Complex &Complex::square() {
    return Complex::multi(this);
}
