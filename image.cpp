#include <iostream>
#include "include/image.h"
#include "include/imageProcessor.h"
#include "include/mandelbrot.h"

void writeImage(image_t &image, const std::string& filePath, bool log) {
    auto mandelbrot = Mandelbrot(2, image.maxIterations);

    if (log) {
        std::cout << "Generating image..." << std::endl;
    }

    auto pixels = mandelbrot.getImage(image.upperLeftBound, image.lowerRightBound, image.width, image.height);

    if (log) {
        std::cout << "Image generated! Processing..." << std::endl;
    }

    ImageProcessor imageProcessor = ImageProcessor(image, pixels);
    imageProcessor.writePngData(filePath);

    if (log) {
        std::cout << "Image complete; written to file." << std::endl;
    }
};