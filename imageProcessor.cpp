#include <iostream>
#include <cmath>
#include "include/imageProcessor.h"
#include "include/palette.h"

bool ImageProcessor::writePngData(const std::string& path) {
    FILE *fp = fopen(path.c_str(), "wb");

    if (!fp) {
        std::cerr << "Could not open file!" << std::endl;
        return false;
    }

    png_structp png = png_create_write_struct(PNG_LIBPNG_VER_STRING, nullptr, nullptr, nullptr);

    if (!png) {
        std::cerr << "Could not initialize the PNG write struct!" << std::endl;
        return false;
    }

    png_infop info = png_create_info_struct(png);

    if (!info) {
        std::cerr << "Could not initialize the PNG info struct!" << std::endl;
        return false;
    }

    // Set the jmpbuf for error handling
    if (setjmp(png_jmpbuf(png))) {
        fclose(fp);
        png_destroy_write_struct(&png, &info);
        return false;
    }

    png_byte bitDepth = 8;
    png_byte colorType = PNG_COLOR_TYPE_RGB;
    auto row = (png_bytep) malloc(sizeof(png_byte) * (image.height * image.width));

    png_init_io(png, fp);
    png_set_IHDR(png,
            info,
            image.width, image.height,
            bitDepth,
            colorType,
            PNG_INTERLACE_NONE,
            PNG_COMPRESSION_TYPE_DEFAULT,
            PNG_FILTER_TYPE_DEFAULT);
    png_write_info(png, info);

    std::vector<pixelColor_t> palette = Palette::getGreenLinearPalette();

    int segmentSize = std::floor(image.maxIterations / static_cast<double>(palette.size()));

    if (segmentSize == 0) {
        segmentSize = 1;
    }

    std::list<pixelData_t>::iterator it;
    int num = 0;
    for (it = pixels.begin(); it != pixels.end(); ++it) {
        if (num % image.width == 0 && num != 0) {
            png_write_row(png, row);
            num = 0;
            continue;
        }
        int index = num * 3;

        if (!it->reachedLimit) {
            row[index] = 0;
            row[index + 1] = 0;
            row[index + 2] = 0;

            num++;
            continue;
        }

        int section = it->iterations / segmentSize;
        pixelColor_t color = palette[section];

        row[index] = color.red;
        row[index + 1] = color.green;
        row[index + 2] = color.blue;

        num++;
    }

    png_write_end(png, info);
    free(row);
    fclose(fp);
    png_destroy_write_struct(&png, &info);

    return true;
}
