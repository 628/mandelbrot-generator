#ifndef MANDELBROTGENERATOR_COMMANDHANDLER_H
#define MANDELBROTGENERATOR_COMMANDHANDLER_H

#include <list>
#include <string>
#include <functional>

// Holds metadata relating to the Mandelbrot image
typedef struct metadataCluster {
    int iterations = 0;
} metadataCluster_t;

/*
 * Holds the data for each command argument, include the callback function 
 * executed for that argument
 */
class CommandArgument {
public:
    std::string fullArgument;
    std::string abbreviation;
    std::function<std::string(metadataCluster_t *, std::string)> callback;

    CommandArgument(std::string &fullArgument,
                    std::string &abbreviation,
                    std::function<std::string(metadataCluster_t *, std::string)> &callback) {
        this->fullArgument = fullArgument;
        this->abbreviation = abbreviation;
        this->callback = callback;
    }
};

// Handles command-line arguments
class CommandHandler {
public:
    CommandHandler(int argc, char **argv) {
        for (int i = 0; i < argc; i++) {
            tokens.emplace_back(argv[i]);
        }
    }

    void registerArgument(std::string fullArgument,
                          std::string abbreviation,
                          std::function<std::string(metadataCluster_t *, std::string)> callback);

    metadataCluster_t &getMetadataFromCommandInput();

private:
    std::list<std::string> tokens;
    std::list<CommandArgument> arguments;
};


#endif //MANDELBROTGENERATOR_COMMANDHANDLER_H
