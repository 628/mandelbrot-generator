#ifndef MANDELBROTGENERATOR_COMPLEX_H
#define MANDELBROTGENERATOR_COMPLEX_H

/*
 * Handles complex numbers and various math operations associated with them.
 */
class Complex {
public:
    long double real, imag;

    Complex(long double real, long double imag) {
        this->real = real;
        this->imag = imag;
    }

    // Returns the absolute value (i.e the distance between the two points on the complex plane)
    double abs();

    // Returns the sum of another complex number with this one
    Complex& add(Complex *complex);

    // Returns the product of another complex number with this one
    Complex& multi(Complex *complex);

    // Returns the squared value of this complex number
    Complex& square();
};


#endif //MANDELBROTGENERATOR_COMPLEX_H
