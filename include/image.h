#ifndef MANDELBROTGENERATOR_IMAGE_H
#define MANDELBROTGENERATOR_IMAGE_H

#include <bits/types/FILE.h>
#include <string>
#include "image_t.h"

/*
 * High-level function that accepts image data via the image_t struct and
 * outputs the resulting image at the given file path. Logging is optional.
 */
void writeImage(image_t image, std::string filePath, bool log);

#endif //MANDELBROTGENERATOR_IMAGE_H
