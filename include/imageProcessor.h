#ifndef MANDELBROTGENERATOR_IMAGEPROCESSOR_H
#define MANDELBROTGENERATOR_IMAGEPROCESSOR_H

#include <png.h>
#include <string>
#include "image_t.h"

/*
 * Transfers the raw data that was previously computed into an
 * image using libpng
 */
class ImageProcessor {
public:
    image_t image;
    std::list<pixelData_t> pixels;

    ImageProcessor(image_t &image, std::list<pixelData_t> &pixels) {
        this->image = image;
        this->pixels = pixels;
    }

    bool writePngData(const std::string& path);
};


#endif //MANDELBROTGENERATOR_IMAGEPROCESSOR_H
