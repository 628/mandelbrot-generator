#ifndef MANDELBROTGENERATOR_IMAGE_T_H
#define MANDELBROTGENERATOR_IMAGE_T_H

#include <vector>
#include "complex.h"
#include "pixeldata.h"
#include <list>

/*
 * Stores data about a Mandelbrot image
 */
typedef struct image_t {
    Complex upperLeftBound = Complex(0, 0);
    Complex lowerRightBound = Complex(0, 0);
    int width = 0;
    int height = 0;
    int maxIterations = 0;
} image_t;

#endif //MANDELBROTGENERATOR_IMAGE_T_H
