#ifndef MANDELBROTGENERATOR_MANDELBROT_H
#define MANDELBROTGENERATOR_MANDELBROT_H

#include "complex.h"
#include "image_t.h"
#include "pixeldata.h"

/*
 * Handles the computation of the Mandelbrot image
 */
class Mandelbrot {
public:
    int limit, iterationLimit;

    Mandelbrot(int limit, int iterationLimit) {
        this->limit = limit;
        this->iterationLimit = iterationLimit;
    }

    // Returns the computed values for all of the pixel in the given bounds.
    std::list<pixelData_t> getImage(Complex &upperRightBound, Complex &lowerLeftBound, int pxWidth, int pxHeight);

private:
    // Returns the pixel data for the given coordinate on the complex plane
    pixelData_t &getData(Complex coord);
};


#endif //MANDELBROTGENERATOR_MANDELBROT_H
