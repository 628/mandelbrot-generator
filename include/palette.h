#ifndef MANDELBROTGENERATOR_PALETTE_H
#define MANDELBROTGENERATOR_PALETTE_H

#include <pngconf.h>
#include <list>

/*
 * Stores the RGB value for a pixel
 */
typedef struct pixelColor {
    png_byte red = 0;
    png_byte green = 0;
    png_byte blue = 0;
} pixelColor_t;

/*
 * Stores palettes used for coloring the resulting Mandelbrot image
 */
class Palette {
public:
    static std::vector<pixelColor_t> getRedLinearPalette();
    static std::vector<pixelColor_t> getBlueLinearPalette();
    static std::vector<pixelColor_t> getGreenLinearPalette();

    static std::vector<pixelColor_t> getRedDoublePalette();
};

#endif //MANDELBROTGENERATOR_PALETTE_H
