#ifndef MANDELBROTGENERATOR_PIXELDATA_H
#define MANDELBROTGENERATOR_PIXELDATA_H

#include "complex.h"

/*
 * Stores data related to a computed pixel
 */
typedef struct pixelData {
    Complex coordinate = Complex(0, 0);
    int iterations = 0;
    bool reachedLimit = false;
} pixelData_t;

#endif //MANDELBROTGENERATOR_PIXELDATA_H
