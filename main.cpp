#include "include/mandelbrot.h"
#include "include/image.h"
#include "include/commandHandler.h"

void registerArguments();

int main() {
    // TODO Replace manual variable manipulation with the command-line arguments.
    int iterations = 500;

    Complex upperLeftBound = Complex(-2.5, 2);
    Complex lowerRightBound = Complex(1.5, -2);

    image_t image = image_t();
    image.lowerRightBound = lowerRightBound;
    image.upperLeftBound = upperLeftBound;
    image.width = 1000;
    image.height = 1000;
    image.maxIterations = iterations;

    writeImage(image, "/home/sandro/mandelbrot.png", true);
}

void registerArguments(CommandHandler &commandHandler) {
    commandHandler.registerArgument(
            "iterations",
            "i",
            [](metadataCluster_t *cluster, std::string val) {
                int iter;
                try {
                    iter = std::stoi(val);
                } catch (const std::invalid_argument&) {
                    return "You must specify a number for the max iteration count!";
                }

                cluster->iterations = iter;
                return "";
            });
}
