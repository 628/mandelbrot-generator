#include "include/mandelbrot.h"
#include "include/complex.h"

std::list<pixelData_t> Mandelbrot::getImage(Complex &upperLeftBound, Complex &lowerRightBound, int pxWidth, int pxHeight) {
    std::list<pixelData_t> pixels;

    long double realScale = (lowerRightBound.real - upperLeftBound.real) / pxWidth;
    long double imagScale = (upperLeftBound.imag - lowerRightBound.imag) / pxHeight;

    auto complex = new Complex(lowerRightBound.real, upperLeftBound.imag);

    for (int j = 0; j <= pxHeight; j++) {
        for (int i = 0; i <= pxWidth; i++) {
            long double real = (realScale * i) + upperLeftBound.real;
            long double imag = (imagScale * j) + lowerRightBound.imag;

            complex->real = real;
            complex->imag = imag;

            auto data = getData(*complex);
            pixels.push_back(data);
        }
    }

    delete complex;
    return pixels;
}

pixelData_t &Mandelbrot::getData(Complex coord) {
    auto data = new pixelData();

    auto value = new Complex(0, 0);
    int iterations = this->iterationLimit;
    bool reachedLimit = false;

    for (int i = 1; i <= this->iterationLimit; i++) {
        value->square().add(&coord);

        if (value->abs() >= this->limit) {
            iterations = i;
            reachedLimit = true;
            break;
        }
    }

    data->coordinate = coord;
    data->iterations = iterations;
    data->reachedLimit = reachedLimit;

    delete value;

    return *data;
}
