#include <vector>
#include "include/palette.h"

std::vector<pixelColor_t> Palette::getRedLinearPalette() {
    std::vector<pixelColor_t> colors;

    png_byte red = 0;
    png_byte green = 0;
    png_byte blue = 0;
    pixelColor_t color = {red, green, blue};
    colors.push_back(color);

    // Increment red; green 0 blue 0
    for (; red < 255; red++) {
        color = {red, green, blue};
        colors.push_back(color);
    }

    colors.push_back(color);

    // Increment green; red 255 blue 0
    for (; green < 255; green++) {
        color = {red, green, blue};
        colors.push_back(color);
    }

    colors.push_back(color);

    // Increment blue; red 255 green 255
    for (; blue < 255; blue++) {
        color = {red, green, blue};
        colors.push_back(color);
    }

    color = {0, 0, 0};
    colors.push_back(color);

    return colors;
}

std::vector<pixelColor_t> Palette::getBlueLinearPalette() {
    std::vector<pixelColor_t> colors;

    png_byte red = 0;
    png_byte green = 0;
    png_byte blue = 0;
    pixelColor_t color = {red, green, blue};
    colors.push_back(color);

    // Increment blue; red 0 green 0
    for (; blue < 255; blue++) {
        color = {red, green, blue};
        colors.push_back(color);
    }

    colors.push_back(color);

    // Increment green; red 0 blue 255
    for (; green < 255; green++) {
        color = {red, green, blue};
        colors.push_back(color);
    }

    colors.push_back(color);

    // Increment red; green 255 blue 255
    for (; red < 255; red++) {
        color = {red, green, blue};
        colors.push_back(color);
    }

    color = {0, 0, 0};
    colors.push_back(color);

    return colors;
}

std::vector<pixelColor_t> Palette::getGreenLinearPalette() {
    std::vector<pixelColor_t> colors;

    png_byte red = 0;
    png_byte green = 0;
    png_byte blue = 0;
    pixelColor_t color = {red, green, blue};
    colors.push_back(color);

    // Increment green; red 0 blue 0
    for (; green < 255; green++) {
        color = {red, green, blue};
        colors.push_back(color);
    }

    colors.push_back(color);

    // Increment blue; red 0 green 255
    for (; blue < 255; blue++) {
        color = {red, green, blue};
        colors.push_back(color);
    }

    colors.push_back(color);

    // Increment red; green 255 blue 255
    for (; red < 255; red++) {
        color = {red, green, blue};
        colors.push_back(color);
    }

    color = {0, 0, 0};
    colors.push_back(color);

    return colors;
}

std::vector<pixelColor_t> Palette::getRedDoublePalette() {
    std::vector<pixelColor_t> colors;

    png_byte red = 0;
    png_byte green = 0;
    png_byte blue = 0;
    pixelColor_t color = {red, green, blue};
    colors.push_back(color);

    // Increment red; green 0 blue 0
    for (; red < 255; red++) {
        color = {red, green, blue};
        colors.push_back(color);
    }

    colors.push_back(color);

    // Increment green; red 255 blue 0
    for (; green < 255; green++) {
        color = {red, green, blue};
        colors.push_back(color);
    }

    colors.push_back(color);

    // Increment blue; red 255 green 255
    for (; blue < 255; blue++) {
        color = {red, green, blue};
        colors.push_back(color);
    }

    colors.push_back(color);

    // Decrement blue; red 255 green 255
    for (; blue > 0; blue--) {
        color = {red, green, blue};
        colors.push_back(color);
    }

    colors.push_back(color);

    // Decrement green; red 255 blue 0
    for (; green > 0; green--) {
        color = {red, green, blue};
        colors.push_back(color);
    }

    colors.push_back(color);

    // Decrement red; green 0 blue 0
    for (; red > 0; red--) {
        color = {red, green, blue};
        colors.push_back(color);
    }

    color = {0, 0, 0};
    colors.push_back(color);

    return colors;
}